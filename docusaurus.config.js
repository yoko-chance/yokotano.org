// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

/** @type {import('@docusaurus/types').Config} */
export default {
  title: 'yokotano',
  tagline: 'ヨコタのつくったものが集まる場所。たぶん',
  favicon: 'img/favicon.ico',

  url: 'https://www.yokotano.org',
  baseUrl: '/',

  organizationName: 'yoko-chance',
  projectName: 'yokotano.org',

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  plugins: [[ require.resolve('docusaurus-lunr-search'), {
    languages: ['ja', 'en']
  }]],

  customFields: {
    externalLinks: [
      {
        url: 'https://gitlab.com/yoko-chance',
        type: 'GitLab',
        title: 'yoko-chance',
      },
      {
        url: 'https://note.com/yoko_chance/m/m92f4c4952e39',
        type: 'note マガジン',
        title: 'あることないこと',
      },
      {
        url: 'https://yokotano.booth.pm/',
        type: 'BOOTH',
        title: 'yokotano',
      },
      {
        url: 'https://www.instagram.com/yokota_daze/',
        type: 'Instagram',
        title: 'yokota_daze',
      },
      {
        url: 'https://www.youtube.com/channel/UC7f7a24dRmeYiqpiHWrIGiw/',
        type: 'YouTube',
        title: 'yoko-chance',
      },
      {
        url: 'https://www.pixiv.net/users/4919295',
        type: 'pixiv',
        title: 'yoko-chance',
      },
      {
        url: 'https://vrc.group/YOKOTA.2365',
        type: 'VRChat Group',
        title: 'ヨコタと',
      },
      {
        url: 'https://discord.gg/Y3AamnN87t',
        type: 'Discord',
        title: 'ヨコタの',
      },
      {
        url: 'https://bsky.app/profile/yokotano.org',
        type: 'Bluesky',
        title: 'ヨコタ',
      },
      {
        url: 'https://misskey.io/@yoko_chance',
        type: 'Misskey',
        title: 'ヨコタ',
      },
    ],
  },

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'ja',
    locales: ['ja'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          routeBasePath: '/',
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: 'https://gitlab.com/yoko-chance/yokotano.org/-/blob/main/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
        blog: {
          blogTitle: 'ヨコタのログ',
          blogDescription: '製作中の進捗とか',
          editUrl: 'https://gitlab.com/yoko-chance/yokotano.org/-/blob/main/',
          postsPerPage: 10,
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      // image: 'img/social-card.jpg', // TODO
      navbar: {
        title: 'yokotano',
        logo: {
          alt: 'yokotano logo',
          src: 'img/icon.png', // TODO
        },
        items: [
          {
            type: 'docSidebar',
            sidebarId: 'works',
            position: 'left',
          },
          {
            to: 'blog',
            label: 'Blog',
            position: 'left'
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Contacts',
            items: [
              { label: 'Discord', href: 'https://discord.com/users/430200088938283019', },
              { label: 'E-mail',  href: 'mailto:r.yokota14@gmail.com', },
            ],
          },
          {
            title: 'Works',
            items: [
              { label: 'Photo',  to: 'category/photo', },
              { label: 'VRChat', to: 'category/vrchat', },
              { label: 'Misc',   to: 'category/misc', },
            ],
          },
        ],
        // logo: { // TODO
        //   alt: 'ヨコタの',
        //   src: 'img/logo.large.png',
        //   href: 'https://example.com',
        //   width: 999,
        //   height: 99,
        // },
        copyright: `Copyright © ${new Date().getFullYear()} , ヨコタ`,
      },
    }),
};
