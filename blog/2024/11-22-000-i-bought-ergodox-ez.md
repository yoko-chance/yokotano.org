---
tags: [ keyboard, ergodox ez ]

---

ErgoDox EZを買いました
=====================
![ErgoDox EZ](https://static.yokotano.org/IMG_8133.jpg)

表題の通り

以前キットのErgoDashを使っていたんですがUSBポートが根元から抜けてしまい、一度修理してグルーガンで固めたもののまた抜けてしまって以来めんどうで修理もせず、さりとて新しいものを買うでもなくたしか誰かにもらったフルキーボードを今は使っています
<!-- truncate -->

ゲーミングPCを買うまでMacbookを使っていたので、当時はわざわざ外部キーボードを使うこともありませんでした  
せっかくゲーミングPCを買うなら以前から気になっていた分割キーボードを試してみようとErgoDashを導入しました

使っていてErgoDashはキーが多いなと感じていたのでどうせなら自作するかと設計を始めるもなかなか難しく、完成してもまた壊れた際にこのフルキーボードを使うのはいやだなということで、一旦製品として完成している分割キーボードをと買ったのがErgoDox EZです

とりあえずキーマップを変更してから使うかと [QMK Configurator](https://config.qmk.fm/) で設定を書き出し、QMK Toolboxで書き込もうとするも、そもそもErgoDox EZを認識してくれない  
[teensy](https://www.pjrc.com/teensy/loader.html) を使ったという記事があったのでこれを試してみるも変わらずでいまだキーマップを変えられていません

[こういうの](https://ergodox-ez.com/flash) があるらしいので試してみます
