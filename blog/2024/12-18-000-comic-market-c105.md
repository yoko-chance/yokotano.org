---
tags: [ たんたん, イベント, 写真, 写真集 ]

---

コミケC105にサークル参加します: 月-東プ41b
==========================================
初サークル参加

[boothにも出しているたんたん](https://yokotano.booth.pm/items/5663704) と新しく刷った"たんたんの2"を持っていきます  
[月曜日の東プ41b](https://webcatalog-free.circle.ms/Circle/20001878) にいますのでよければお立ち寄りください

<!-- truncate -->

たんたん 1500円  
![たんたん表紙](https://static.yokotano.org/tan-tan/01.hyoshi.jpg)  
![たんたん中身](https://static.yokotano.org/tan-tan/01.nakami.jpg)

たんたんの2 1500円  
![たんたんの2表紙](https://static.yokotano.org/tan-tan/02.hyoshi.jpg)  
![たんたんの2中身](https://static.yokotano.org/tan-tan/02.nakami.jpg)
