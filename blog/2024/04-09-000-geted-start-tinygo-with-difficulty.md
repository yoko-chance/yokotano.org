---
tags: [ golang, tinygo, arduino, wsl ]

---

TinyGoを始めるのに苦労しました
==============================
windows10のWSLでTinyGoを使ってArduino UNOに書き込みをしようとしたらそれなりに嵌りました。
<!-- truncate -->

はじまり
--------
[とりあえず公式ドキュメントを確認](https://tinygo.org/getting-started/install/linux/) して必要なものをubuntuでダウンロードした。  

LEDやら用意するのが面倒だったので、 [チュートリアルをみつつ](https://tinygo.org/docs/tutorials/serialmonitor/) シリアル出力でhello worldを行おうとした。
書き込み時のオプションは [公式リファレンスで確認](https://tinygo.org/docs/reference/microcontrollers/arduino/) して指定した。

使用したソースは以下の通り(チュートリアルのまま)

```go
// embed/main.go
package main

import (
	"time"
)

func main() {
	count := 0
	for {
		println(count, ": Hello, World")
		time.Sleep(time.Millisecond * 1000)
		count++
	}
}
```

書き込みコマンドは以下の通り。

```sh
tinygo flash -target arduino embed/main.go
```

実行すると以下のエラーが発生した。

```log
error: unable to locate a serial port
```

sudoでも同様だった。  
Arduino IDEをwindowsにインストールして書き込みをしてみるとこれは成功したのでwslでなにかあるのだろうという前提で調べ始めた。

検索
----
wslではwindows上のCOMポートを `/dev/ttyS<n>` に繋げているらしいことは知っていたので、デバイスモニターでArduinoがつながっているポートを確認すると `COM3` だったので wsl上では `/dev/ttyS3` に繋がるんだろうと理解した。

とりあえずポートを明示してみたがダメ。

```sh
tinygo flash -target arduino -p /dev/ttyS3 embed/main.go
```

windowsとwslでなにかうまく繋がってないんだろうなと思っていたが、なかなかそんな感じの情報が出てこない。  
ドライバーが入ってないとか、解決せずにクローズされたイシューばかり。

やっと求めてた情報が以下のページで見つけられた  
https://qiita.com/KMNMKT/items/ae0e76a3e420b3ca16e6

どこみてもUSBをwslで認識させる手順を書いたページを見つけられなかったので、最初から繋がってるもんだと思っていたがどうやら自分で繋げないと行けなかったらしい。  
作成時期が古かったのでコマンドは少し違ったがとにかくこれでwslからポートを確認できるようになった。

```sh
usbipd list # 4-1にArduinoが接続されていることを確認した
usbipd bind -b 4-1
usbipd attach --wsl --busid 4-1 # ここでwslのバージョンが古いと起こられたので `wsl --update` を実施した
```

`usbipd attach` はwslを起動した状態で行う必要があるらしい。  
上記を実施すると以下のデバイスが確認できた。

```log
#INS ❯ ls /dev/ttyACM0
/dev/ttyACM0
```

`sudo chmod 666 /dev/ttyACM0` で書き込み出来ようにした上で改めて `tinygo flash -target arduino embed/main.go` を実行するとめでたく書き込みが成功した。  
`tinygo monitor` で期待通り動作もしていことが確認できた。

```log
#CMD ❯ tinygo monitor -target arduino -baudrate=9600
Connected to /dev/ttyACM0. Press Ctrl-C to exit.
5 : : Hello, World
0 : Hello, World
1 : Hello, World
2 : Hello, World
3 : Hello, World
4 : Hello, World
```

残った課題として `usbipd attach` はどうもArduinoを挿すときに毎回しないといけないっぽいのでこれをどうにかしたい。
