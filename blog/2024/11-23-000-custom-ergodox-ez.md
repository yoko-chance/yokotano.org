---
tags: [ keyboard, ergodox ez ]

---

ErgoDox EZをカスタムできた
=========================
できた

[ここの通りでやったらマッピング変更できた](https://www.zsa.io/flash)  
[マッピング設定するページへのリンク](https://configure.zsa.io/) が分かりにくかった以外は特に困ることもなくできた
<!-- truncate -->

winでmacみたいな英/かな切り替えするのは面倒なイメージがあったけど、いつのまにかLANG_1/LANG_2でできるようになってたみたいで驚いた

あとついでにキースイッチも変えた  
キースイッチを外すための工具はErgoDox EZについてきたのでキースイッチだけ買えばできた

キースイッチは低くしたかったのでロープロファイル風の [Tecsee Medium Switch / Linear](https://shop.yushakobo.jp/products/7044?srsltid=AfmBOorjp3L9FftX_NXhRDXZ7BbXldhtbKhlxrCg6Khrdq2EmbcX2dF7) にした

![](https://static.yokotano.org/IMG_8135.jpg)
![](https://static.yokotano.org/IMG_8136.jpg)

しばらくは慣れるための修業期間
