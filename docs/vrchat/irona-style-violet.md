---
tags: [ vrchat avatar, 3d ]
image: https://static.yokotano.org/VRChat_1920x1080_2021-08-27_21-32-07.710.jpg

---

Mode Irona style yoko-chance violet
===================================
2021年に開催された [モード・イロナ (Mode Irona) - VRChat 向け 3Dアバターモデル](https://booth.pm/ja/items/2016092) の改変コンペ"Mode Irona カスタムコンペティション"で優秀賞を頂きました

![アバター立ち姿](https://static.yokotano.org/VRChat_1920x1080_2021-09-26_17-58-25.243.png)

2021年7月21日に開催されたコンペティション当日の様子はYouTubeの3つのチャンネルで配信されました
- [ウィンターズ - Winterz.: 賞金5万のアバター改変コンテストに参加する 【Mode Irona カスタムコンペティション】](https://www.youtube.com/live/W5W6SzT86qs?si=hYVTqQrqhHGSzVWz&t=5362)
- [ドコカノうさぎ VR Idol: 【生配信】改変イロナちゃんファッションショー in VRChat【Mode Irona カスタムコンペ ドコカノうさぎサイド配信】](https://www.youtube.com/live/rPdx7AhZ6uY?si=jv4N9h_NdDKDITlf&t=5089)
- [おきゅたんbotのVRワンダーライフ☆: 【アバター改変コンテスト】こんなに変わる！ モード・イロナ Avatar Customize Competition Live#368](https://www.youtube.com/live/kvixxA2E1MY?si=AenLwBKv2VBfxdZ3&t=5312)

Mode IronaはVRChatでavatarとして利用されることを想定されたモデルです  
同モデルには1,000を超えるシェイプキーが用意されておりUnity上での操作だけでまったく異なる印象を持たせることが可能です

私が改変したアバターは星輝子(アイドルマスター)やホミカ(ポケットモンスターブラック2/ホワイト2)のようなかわいいパンクロックをデザインソースとしています  
最優秀賞は公式の改変例としてパッケージに同梱されるということだったので、既存の改変例と色が被らないよう紫+黒をベースにしました

特徴的なちょんまげはポニーテールやサイドテールとしての利用を想定されていたパーツを前に配置して、さらに前髪の先端を顔のメッシュへ埋め込むことで実現しています

![夜景をバックにしたアバター](https://static.yokotano.org/VRChat_1920x1080_2021-10-20_21-04-08.641.jpg)

表情セットはライブ中にされるようなものを想定して作成しています  
歌唱中のキリっとした顔、おどけたベロだし、オーディエンスを煽る表情などの中に、一つだけバラードの歌唱中のしとやかな表情があります  
これは普段と違う表情にドキッとするような光景をイメージして置いています

![](https://static.yokotano.org/VRChat_1920x1080_2021-07-14_20-53-32.510.jpg)
![](https://static.yokotano.org/VRChat_1920x1080_2021-08-27_21-32-07.710.jpg)

アバターは以下から利用できます  
[mode irona style yoko-chance violet](https://vrchat.com/home/avatar/avtr_3023da73-bfd7-4b0f-9675-71f5d3a0e1da)

ペデスタルは [Himiko Avatar World](https://vrchat.com/home/launch?worldId=wrld_24009ebc-87e4-40a0-ad24-bc11cb3c0cf4) などに設置されています


![](https://static.yokotano.org/VRChat_1920x1080_2021-08-02_19-27-45.673.jpg)
