.DEFAULT_GOAL := help
DEST_DIR      ?= $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))/built

setup: install ## 実行環境をセットアップする

install:
	@npm i

dev: ## 開発用サーバを立ち上げる
	npm run start

build: prepare_dest_dir ## ビルドする
	npm run build -- --out-dir $(DEST_DIR)

prepare_dest_dir: ## 成果物を配置するディレクトリを作成する
	@mkdir -p $(DEST_DIR)

clean: ## 作成したファイルを削除する
	@rm -rf $(DEST_DIR)

help: ## display this text. refer to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-10s\033[0m : %s\n", $$1, $$2}'
