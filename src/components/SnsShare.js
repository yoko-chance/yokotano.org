export function TwitterShareButton({ text, url }) {
  const text_encoded = encodeURIComponent(text);
  const url_encoded  = encodeURIComponent(url);

  return (
    <>
      <a
        href={`https://twitter.com/intent/tweet?ref_src=twsrc%5Etfw&text=${text_encoded}%20%23yokotano%0D%0A&url=${url_encoded}`}
        className="twitter-share-button"
        target="_blank"
        data-show-count="false"
      >Share to X</a>
      <script async src="https://platform.twitter.com/widgets.js" charSet="utf-8"></script>
    </>
  );
}
