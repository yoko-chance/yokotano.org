import React from 'react';
import Footer from '@theme-original/BlogPostItem/Footer';
import {TwitterShareButton} from '/src/components/SnsShare';

import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import {useBlogPost} from '@docusaurus/plugin-content-blog/client';

export default function FooterWrapper(props) {
  const { siteConfig } = useDocusaurusContext();
  const { metadata, isBlogPostPage } = useBlogPost();

  const title       = metadata.title;
  const url_current = `${siteConfig.url}${metadata.permalink}`;

  return (
    <>
      { isBlogPostPage && <div style={{ margin: '3rem 0 -2rem' }}>
        <TwitterShareButton text={title} url={url_current} />
      </div> }

      <Footer {...props} />
    </>
  );
}
