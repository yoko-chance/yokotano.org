import React from 'react';
import Footer from '@theme-original/DocItem/Footer';
import {TwitterShareButton} from '/src/components/SnsShare';

import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import {useDoc} from '@docusaurus/plugin-content-docs/client';

export default function FooterWrapper(props) {
  const { siteConfig } = useDocusaurusContext();
  const { metadata } = useDoc();

  const title       = metadata.title;
  const url_current = `${siteConfig.url}${metadata.permalink}`;

  return (
    <>
      <div style={{ margin: '3rem 0 -2rem' }}>
        <TwitterShareButton text={title} url={url_current} />
      </div>

      <Footer {...props} />
    </>
  );
}
