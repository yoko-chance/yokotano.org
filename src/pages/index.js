import React from 'react';
import clsx from 'clsx';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';

import styles from './index.module.css';

import { Container, Box, ButtonBase } from '@mui/material';
import Grid from '@mui/material/Unstable_Grid2';
import { styled } from '@mui/material/styles';

export default function Home() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={siteConfig.title}
      description={siteConfig.tagline}>
      <HomeHeader />
      <HomeContent />
    </Layout>
  );
}

function HomeHeader() {
  const {siteConfig} = useDocusaurusContext();

  return (
    <header className={clsx('hero', styles.heroBanner)}>
      <Container>
        <h1 className="hero__title">{siteConfig.title}</h1>
        <p className="hero__subtitle">{siteConfig.tagline}</p>
        <CategoryLink category='works' label='Works'/>
      </Container>
    </header>
  );
}

function HomeContent() {
  const {siteConfig} = useDocusaurusContext();
  const sectionStyle = {
    'padding': '3rem 0'
  }

  return (
    <section style={sectionStyle}><Container maxWidth="md">
      <Box textAlign='center'>
        <p>以下のリンク先でなにかしらしてます</p>
      </Box>
      <ExternalLinks links={siteConfig.customFields.externalLinks}/>
    </Container></section>
  );
}

function CategoryLink({category, label}) {
  return (
      <Link
        className="button button--secondary button--lg"
        to={'/category/'+category}>
        {label}
      </Link>
  );
}

function ExternalLinks({links}) {
  const ExternalLinkButton = styled(ButtonBase)({
    fontSize: '1.3rem',
    fontWeight: 'bold',
    textTransform: 'none',
  });

  const LinkInner = ({type, title}) => {
    const LinkTypePrefix = styled('span')({
      display: 'inline-block',
      paddingRight: '0.5rem',
      '&:after': {
        content: '":"',
      },
    });

    const LinkTitle = styled('span')({
      display: 'inline-block',
    });

    return (
      <span>
        <LinkTypePrefix>{type}</LinkTypePrefix>
        <LinkTitle>{title}</LinkTitle>
      </span>
    );
  };

  return (
    <Grid container spacing={3} columns={{ xs: 4, sm: 8, md: 12 }}>
      {links.map((props, index) => (
        <Grid xs={2} sm={4} md={4} key={index}>
          <Box textAlign='center'>
            <ExternalLinkButton href={props.url} variant="contained" size="large" color="primary">
              <LinkInner type={props.type} title={props.title}/>
            </ExternalLinkButton>
          </Box>
        </Grid>
      ))}
    </Grid>
  );
}
