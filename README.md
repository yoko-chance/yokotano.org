ヨコタの
========
https://www.yokotano.org/  
ヨコタのなにかが集められる場所

docusaurus使って実装
- https://docusaurus.io/
- https://docusaurus.io/docs/api/plugins

Installation
------------
```sh
make setup
```

Local Development
-----------------
```sh
make dev
```

Build
-----
```sh
make build
```

Deployment
----------
TODO
